# Copies images from the C:/The_High_Resolution_Directory to C:/The_Destination_Directory if the file(name) is already present in C:/The_Low_Resolution_Directory

import os
import sys
import shutil

def main(argv):
	
	# Update current working directory
	os.chdir(os.path.dirname(sys.argv[0]))

	# Variables
	basepath = os.path.dirname(__file__)
	orginalDirectory = "C:/The_High_Resolution_Directory"
	compareDirectory = "C:/The_Low_Resolution_Directory"
	destinationDirectory = "C:/The_Destination_Directory"
	
	count = 0
	
	files = os.listdir(orginalDirectory)
	
	# Loop through 
	for file in files:
		
		comparePath = os.path.join(compareDirectory, file)
		if os.path.isfile(comparePath):
				
			count = count + 1
			
			# Copy file to destinationDirectory
			orginalPath = os.path.join(orginalDirectory, file)
			print "Copying, " + orginalPath
			shutil.copy(orginalPath, destinationDirectory)
	
	print "Total files: " + str(count)
	
if __name__ == "__main__":
   main(sys.argv[1:])