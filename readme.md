#PicDuplicator

**********************************************************************
     
Copies files from a high resolution directory to a destination directory, if a file of the same name is already present in the low resolution directory     
     
Useful when the client has provided you with a set of low res selected pics, and need the high res to be delivered.      
     